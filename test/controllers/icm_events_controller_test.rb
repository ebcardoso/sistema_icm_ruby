require 'test_helper'

class IcmEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @icm_event = icm_events(:one)
  end

  test "should get index" do
    get icm_events_url
    assert_response :success
  end

  test "should get new" do
    get new_icm_event_url
    assert_response :success
  end

  test "should create icm_event" do
    assert_difference('IcmEvent.count') do
      post icm_events_url, params: { icm_event: { description: @icm_event.description, event_date: @icm_event.event_date, limit_reg: @icm_event.limit_reg, name: @icm_event.name } }
    end

    assert_redirected_to icm_event_url(IcmEvent.last)
  end

  test "should show icm_event" do
    get icm_event_url(@icm_event)
    assert_response :success
  end

  test "should get edit" do
    get edit_icm_event_url(@icm_event)
    assert_response :success
  end

  test "should update icm_event" do
    patch icm_event_url(@icm_event), params: { icm_event: { description: @icm_event.description, event_date: @icm_event.event_date, limit_reg: @icm_event.limit_reg, name: @icm_event.name } }
    assert_redirected_to icm_event_url(@icm_event)
  end

  test "should destroy icm_event" do
    assert_difference('IcmEvent.count', -1) do
      delete icm_event_url(@icm_event)
    end

    assert_redirected_to icm_events_url
  end
end
