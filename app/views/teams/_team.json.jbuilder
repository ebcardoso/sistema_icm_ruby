json.extract! team, :id, :name, :leader, :created_at, :updated_at
json.url team_url(team, format: :json)
