json.extract! icm_event, :id, :name, :description, :event_date, :limit_reg, :created_at, :updated_at
json.url icm_event_url(icm_event, format: :json)
