json.extract! church, :id, :name, :code, :address, :district, :city, :state, :number, :c_type, :created_at, :updated_at
json.url church_url(church, format: :json)
