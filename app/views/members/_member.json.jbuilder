json.extract! member, :id, :name, :name_abv, :phone, :date_of_birth, :is_disable, :church_id, :created_at, :updated_at
json.url member_url(member, format: :json)
