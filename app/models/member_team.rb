class MemberTeam < ApplicationRecord
	belongs_to :member, required: true
	belongs_to :team, required: true
end
