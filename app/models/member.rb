class Member < ApplicationRecord
	has_many :member_teams
	belongs_to :church
end
