class IcmEventsController < ApplicationController
  before_action :set_icm_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  # GET /icm_events
  # GET /icm_events.json
  def index
    @icm_events = IcmEvent.all
    @title_section = "Eventos"
    @title = @title_section
  end

  # GET /icm_events/1
  # GET /icm_events/1.json
  def show
    @title_section = @icm_event.name
    @title = "Tipo: " + @title_section
  end

  # GET /icm_events/new
  def new
    @icm_event = IcmEvent.new
    @title_section = "Cadastro de Evento"
    @title = @title_section
  end

  # GET /icm_events/1/edit
  def edit
    @title_section = "Editando: " + @icm_event.name
    @title = @title_section
  end

  # POST /icm_events
  # POST /icm_events.json
  def create
    @icm_event = IcmEvent.new(icm_event_params)

    respond_to do |format|
      if @icm_event.save
        format.html { redirect_to @icm_event, notice: 'Icm event was successfully created.' }
        format.json { render :show, status: :created, location: @icm_event }
      else
        format.html { render :new }
        format.json { render json: @icm_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /icm_events/1
  # PATCH/PUT /icm_events/1.json
  def update
    respond_to do |format|
      if @icm_event.update(icm_event_params)
        format.html { redirect_to @icm_event, notice: 'Icm event was successfully updated.' }
        format.json { render :show, status: :ok, location: @icm_event }
      else
        format.html { render :edit }
        format.json { render json: @icm_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /icm_events/1
  # DELETE /icm_events/1.json
  def destroy
    @icm_event.destroy
    respond_to do |format|
      format.html { redirect_to icm_events_url, notice: 'Icm event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_icm_event
      @icm_event = IcmEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def icm_event_params
      params.require(:icm_event).permit(:name, :description, :event_date, :limit_reg)
    end
end
