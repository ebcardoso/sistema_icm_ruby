class MemberTeamsController < ApplicationController
	before_action :set_member_team, only: [:unsubscribe_member]
	before_action :set_former_members, only: [:former_members]
	before_action :set_team, only: [:former_members]
	before_action :authenticate_user!

	def insert_member_page
		@title = "Inserir Membro"
		@title_section = "Inserir Membro na Equipe"
		
		@members = Member.where.not({id: MemberTeam.select(:member_id).where(active: true)}).order(:name)
		@member_team = MemberTeam.new
	end

	def insert_member
		@mt = MemberTeam.new
			@mt.member_id = member_team_params[:member_id]
			@mt.team_id = params[:id]
			@mt.date_start = DateTime.now.to_date

		respond_to do |format|
			if @mt.save
				format.html { redirect_to team_path(params[:id]), notice: 'Membro Cadastrado com Sucesso' }
				#format.json { render :show, status: :created, location: @team }
			else
				format.html { redirect_to insert_member_path(params[:id]), notice: @mt.errors}
				#format.json { render json: @mt.errors, status: :unprocessable_entity }
			end
		end	
		#redirect_to team_path(params[:id])
	end

	def unsubscribe_member
		@mt.date_finish = DateTime.now.to_date
		@mt.active = false
		respond_to do |format|
			if @mt.save
				format.html { redirect_to team_path(params[:id]), notice: 'Vínculo Encerrado com Sucesso' }
				#format.json { render :show, status: :created, location: @team }
			else
				format.html { redirect_to team_path(params[:id]), notice: @mt.errors}
				#format.json { render json: @mt.errors, status: :unprocessable_entity }
			end
		end	
	end

	def former_members
		@title = "Membros Antigos: " + @team.name
		@title_section = @title
	end




	private
		def member_team_params
			params.require(:member_team).permit(:member_id)
		end

		def set_member_team
			@mt = MemberTeam.find(params[:id_subscribe])
		end

		def set_team
			@team = Team.find(params[:id])
		end

		def set_former_members
			@member_teams = MemberTeam.all.where('team_id = ? AND active = ?', params[:id], false)
		end
end