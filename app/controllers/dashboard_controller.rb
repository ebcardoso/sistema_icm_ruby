class DashboardController < ApplicationController
	before_action :authenticate_user!
	def index
		@title = "Painel de Controle"
		@title_section = "Painel de Controle"
		@n_events = IcmEvent.count
		@n_members = Member.count
		@n_teams = Team.count
		@n_churches = Church.count
	end
end
