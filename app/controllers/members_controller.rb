class MembersController < ApplicationController
  before_action :set_member, only: [:show, :edit, :update, :destroy]
  before_action :set_church, only: [:edit, :update, :new]  
  before_action :authenticate_user!

  # GET /members
  # GET /members.json
  def index
    @members = Member.all.order(:name_abv)
    @title_section = "Membros"
    @title = @title_section
  end

  # GET /members/1
  # GET /members/1.json
  def show
    @title_section = "Membro: " + @member.name
    @title = @title_section
  end

  # GET /members/new
  def new
    @member = Member.new
    @title_section = "Cadastro de Membro"
    @title = @title_section
  end

  # GET /members/1/edit
  def edit
    @title_section = "Editando: " + @member.name
    @title = @title_section
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)

    respond_to do |format|
      if @member.save
        format.html { redirect_to @member, notice: 'Member was successfully created.' }
        format.json { render :show, status: :created, location: @member }
      else
        format.html { render :new }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    respond_to do |format|
      if @member.update(member_params)
        format.html { redirect_to @member, notice: 'Member was successfully updated.' }
        format.json { render :show, status: :ok, location: @member }
      else
        format.html { render :edit }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @member.destroy
    respond_to do |format|
      format.html { redirect_to members_url, notice: 'Member was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
      params.require(:member).permit(:name, :name_abv, :phone, :date_of_birth, :is_disable, :church_id)
    end

    def set_church
      @churches = Church.all.order(:name)
    end
end
