Rails.application.routes.draw do
	root "dashboard#index"

	get "dashboard" => "dashboard#index"
	get "index" => "dashboard#index"
	get "dashboard/index" => "dashboard#index"
	#get 'welcome/index'

	#insert member in a team
	post "/dashboard/teams/:id/insert_member/" => "member_teams#insert_member", as: "insert_member_team"
	get "/dashboard/teams/:id/insert_member/" => "member_teams#insert_member_page", as: "insert_member"

	#delete member of a team
	patch "/dashboard/teams/:id/unsubscribe/:id_subscribe" => "member_teams#unsubscribe_member", as: "unsubscribe_member"

	#former members
	get "/dashboard/teams/:id/former_members/" => "member_teams#former_members", as: "former_members"

	devise_for :users
	resources :members, path: '/dashboard/members'
	resources :teams, path: '/dashboard/teams'
	resources :icm_events, path: '/dashboard/icm_events'
	resources :churches, path: '/dashboard/churches'
end