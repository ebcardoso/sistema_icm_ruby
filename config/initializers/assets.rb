# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

Rails.application.config.assets.precompile += 
  %w(*.png *.jpg *.jpeg *.gif  \
     vendor/bootstrap/*.js vendor/bootstrap/*.css \
     vendor/bootstrap/**/*.js vendor/bootstrap/**/*.css)

Rails.application.config.assets.precompile += %w( dashboard/vendor/bootstrap/css/bootstrap.min.css )
Rails.application.config.assets.precompile += %w( dashboard/vendor/metisMenu/metisMenu.min.css )
Rails.application.config.assets.precompile += %w( dashboard/vendor/datatables-plugins/dataTables.bootstrap.css )
Rails.application.config.assets.precompile += %w( dashboard/vendor/datatables-responsive/dataTables.responsive.css )
Rails.application.config.assets.precompile += %w( dashboard/dist/css/sb-admin-2.css )
Rails.application.config.assets.precompile += %w( dashboard/vendor/font-awesome/css/font-awesome.min.css )

Rails.application.config.assets.precompile += %w( dashboard/vendor/jquery/jquery.min.js )
Rails.application.config.assets.precompile += %w( dashboard/vendor/bootstrap/js/bootstrap.min.js )
Rails.application.config.assets.precompile += %w( dashboard/vendor/metisMenu/metisMenu.min.js )
Rails.application.config.assets.precompile += %w( dashboard/vendor/datatables/js/jquery.dataTables.min.js )
Rails.application.config.assets.precompile += %w( dashboard/vendor/datatables-plugins/dataTables.bootstrap.min.js )
Rails.application.config.assets.precompile += %w( dashboard/vendor/datatables-responsive/dataTables.responsive.js )
Rails.application.config.assets.precompile += %w( dashboard/dist/js/sb-admin-2.js )

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
