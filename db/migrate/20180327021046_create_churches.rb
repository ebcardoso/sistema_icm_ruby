class CreateChurches < ActiveRecord::Migration[5.1]
  def change
    create_table :churches do |t|
      t.string :name
      t.string :code
      t.string :address
      t.string :district
      t.string :city
      t.string :state
      t.string :number
      t.string :c_type
      t.string :zip

      t.timestamps
    end
    add_index :churches, :code, unique: true
  end
end