class CreateMemberTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :member_teams do |t|
      t.references :member, foreign_key: true
      t.references :team, foreign_key: true
      t.date :date_start
      t.date :date_finish

      t.timestamps
    end
  end
end