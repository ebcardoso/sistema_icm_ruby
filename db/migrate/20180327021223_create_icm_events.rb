class CreateIcmEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :icm_events do |t|
      t.string :name
      t.string :description
      t.date :event_date
      t.date :limit_reg

      t.timestamps
    end
  end
end
