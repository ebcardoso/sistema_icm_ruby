class CreateMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :members do |t|
      t.string :name
      t.string :name_abv
      t.string :phone
      t.date :date_of_birth
      t.boolean :is_disable
      t.references :church, foreign_key: true

      t.timestamps
    end
  end
end
