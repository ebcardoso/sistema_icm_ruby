class AddActiveToMemberTeams < ActiveRecord::Migration[5.1]
  def change
    add_column :member_teams, :active, :boolean, default: true
  end
end
