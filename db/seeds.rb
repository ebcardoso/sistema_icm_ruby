Church.delete_all
UserType.delete_all
Team.delete_all

#Polo-Igapó
Church.create(
	name: "ICM-IGAPÓ",
	code: "0101",
	address: "Rua Santa Luzia",
	district: "Igapó",
	city: "Natal",
	state: "RN",
	number: "409",
	c_type: "Igreja",
	zip: "59.104-290"
)

Church.create(
	name: "ICM-SANTARÉM",
	code: "0102",
	address: "Avenida Rio Doce",
	district: "Potengi",
	city: "Natal",
	state: "RN",
	number: "2761",
	c_type: "Igreja",
	zip: "59.129-340"
)

Church.create(
	name: "ICM-PAJUÇARA",
	code: "0103",
	address: "Rua Marquês de Abrante",
	district: "Pajuçara",
	city: "Natal",
	state: "RN",
	number: "226",
	c_type: "Igreja",
	zip: "59.131-300"
)

Church.create(
	name: "ICM-NOVA NATAL",
	code: "0104",
	address: "Rua do Sequilho",
	district: "Lagoa Azul",
	city: "Natal",
	state: "RN",
	number: "1110-1128",
	c_type: "Igreja",
	zip: "59.139-270"
)

Church.create(
	name: "ICM-ALVORADA",
	code: "0105",
	address: "Rua José Luiz da Silva",
	district: "Nossa Senhora da Apresentação",
	city: "Natal",
	state: "RN",
	number: "1327",
	c_type: "Trabalho",
	zip: "59.114-230 "
)

Church.create(
	name: "ICM-CEARÁ MIRIM",
	code: "0106",
	address: "Rua Assu",
	district: "Ceará-Mirim",
	city: "Ceará-Mirim",
	state: "RN",
	number: "177",
	c_type: "Igreja",
	zip: "59.570-000"
)

#Polo-Mirassol
Church.create(
	name: "ICM-MIRASSOL",
	code: "0201",
	address: "Rua das Camélias",
	district: "Capim Macio",
	city: "Natal",
	state: "RN",
	number: "269",
	c_type: "Igreja",
	zip: "59.078-230"
)

Church.create(
	name: "ICM-ALECRIM",
	code: "0202",
	address: "Rua Alexandrino de Alencar",
	district: "Alecrim",
	city: "Natal",
	state: "RN",
	number: "646",
	c_type: "Igreja",
	zip: "59.030-350"
)

Church.create(
	name: "ICM-SATÉLITE",
	code: "0203",
	address: "Rua Ituaçu",
	district: "Pitimbu",
	city: "Natal",
	state: "RN",
	number: "15",
	c_type: "Igreja",
	zip: "59.069-490"
)

Church.create(
	name: "ICM-NOVA PARNAMIRIM",
	code: "0204",
	address: "Avenida Abel Cabral",
	district: "Nova Parnamirim",
	city: "Parnamirim",
	state: "RN",
	number: "1923",
	c_type: "Igreja",
	zip: "59.151-250"
)

Church.create(
	name: "ICM-MOSSORÓ",
	code: "0205",
	address: "Rua Epitácio Pessoa",
	district: "Bom Jardim",
	city: "Mossoró",
	state: "RN",
	number: "485",
	c_type: "Igreja",
	zip: "59.618-730"
)

Church.create(
	name: "ICM-MORRO BRANCO",
	code: "0206",
	address: "Rua Rui Barbosa",
	district: "Lagoa Nova",
	city: "Natal",
	state: "RN",
	number: "19",
	c_type: "Trabalho",
	zip: "59.060-300"
)

#Polo Parnamirim
Church.create(
	name: "ICM-PARNAMIRIM",
	code: "0301",
	address: "Avenida Comandante Petit",
	district: "Centro",
	city: "Parnamirim",
	state: "RN",
	number: "213",
	c_type: "Igreja",
	zip: "59.140-190"
)

Church.create(
	name: "ICM-SANTA TEREZA",
	code: "0302",
	address: "Rua Sebastiana Constância da Silva",
	district: "Santa Tereza",
	city: "Parnamirim",
	state: "RN",
	number: "213",
	c_type: "Trabalho",
	zip: "59.142-180"
)

#Polo Goianinha
Church.create(
	name: "ICM-GOIANINHA",
	code: "0401",
	address: "Rua Maria da Glória Chaves",
	district: "Vila Helena",
	city: "Goianinha",
	state: "RN",
	number: "178",
	c_type: "Igreja",
	zip: "59.142-180"
)

Church.create(
	name: "ICM-NOVA CRUZ",
	code: "0402",
	address: "Rua Franco de Oliveira",
	district: "Nova Cruz",
	city: "Nova Cruz",
	state: "RN",
	number: "267",
	c_type: "Igreja",
	zip: "59.215-000"
)

#Polo Maanaim
Church.create(
	name: "MAANAIM-RN",
	code: "0501",
	address: "Distrito Industrial",
	district: "Distrito Industrial",
	city: "Macaíba",
	state: "RN",
	number: "-",
	c_type: "Maanaim",
	zip: "59.280-000"
)

#Tipos de Membros
UserType.create(name:"Administrador", code:"icm_adm")
UserType.create(name:"Equipe de Secretaria", code:"icm_eq_sec")
UserType.create(name:"Secretário de Equipe", code:"icm_sec_eq")

#Equipe
Team.create(name:"Grupo de Louvor", leader:"Ung. Luando Lucido")
Team.create(name:"Cozinha", leader:"Pr. Sóstenes Palhares")
Team.create(name:"Limpeza", leader:"Pr. Gilson Câmara")
Team.create(name:"Secretaria", leader:"Pr. Breno")
Team.create(name:"Segurança", leader:"Pr. Altemir")